import RPi.GPIO as GPIO
import logging
import reusables

# Import the ADS1x15 analog board module.
import Adafruit_ADS1x15

GPIO.setmode(GPIO.BCM)# #GPIO Init

class class_input:
    """
    Manages input channel
    """
    def __init__(self,pin,debug=False,use_pud=True,pudup=True,trig='rising'):
        """
        Defined the input class for raspberry direct gpio
        :param pin: numerical pin in BCM mode
        :param debug: T/F extra display
        :param use_pud: T/F Use push down (internal of Pi)
        :param pudup: Type of push down : T:Push Up F:Push Down
        """
        self.pin=pin
        self.log = reusables.get_logger('in '+ str(self.pin), level=logging.DEBUG)
        self.debug=debug
        self.type="btn"
        self.err=0
        self.trig=trig
        if self.debug:
            self.log.debug("trigger :"+self.trig)

        if pudup:
            pud=GPIO.PUD_UP
            if self.debug:
                self.log.debug("push up")
        else :
            pud=GPIO.PUD_DOWN
            if self.debug:
                self.log.debug("push down")

        try :
            if use_pud:
                GPIO.setup(pin,GPIO.IN,pull_up_down=pud)
            else :
                GPIO.setup(pin, GPIO.IN)

            if(self.debug):
                self.log.debug("pinin allocated: "+str(self.pin))


        except :
            self.log.debug("GPIO ERROR with pinin : " +str(self.pin))
            self.err=1

    def state(self):
        """
        Return state input
        :return: T/F
        """
        if self.trig=='falling':
            return GPIO.input(self.pin)
        elif self.trig=='raising':
            return not GPIO.input(self.pin)
        else :
            self.log.error("trigger unknown")


class class_analog:

    def __init__(self,debug=False,threshold=[{'value':0,'unit':'V','abs':True}]):

        self.log = reusables.get_logger('analog ', level=logging.DEBUG)
        self.debug=debug
        if not isinstance(threshold, list):
            threshold=[threshold]
        self.threshold=threshold
        self.channel=0 # Will be overwritten by board subclass
        self.type='analog'
    def listoutput(self,volts=True):
        """
        Return the values of all the channels
        :param volts: In volts or in bytes
        :return: List
        """
        values=[]
        for c in range(self.channel):
            values.append(self.read(channel=c,volts=volts))
        return values

    def state(self,channel):
        """
        Return a boolean value of the channel according to threshold
        :param channel: desired channel
        :return: True if higher than threshold
        """
        if channel < self.channel:
            thr=self.threshold[channel]
            if thr['unit']=='V':
                volts=True
            else :
                volts=False
            V=self.read(channel=channel,volts=volts)
            if  V>= thr['value'] or (thr['abs'] and abs(V) >= thr['value']):
                return True
            else :
                return False

        else :
            self.log.error("Error in channel")
            return False

    def read(self,channel,volts):
        """
        Should be implented in the board subclass
        :param channel:
        :param volts:
        :return:
        """
        self.log.error("Error calling read function")
        return self

class class_analog_ADS1115(class_analog):
    """
    Class for managing ADS1x15 analog 4 channel board
    """
    def __init__(self,gain=(1,1,1,1),usage=(-1,-1,-1,-1),**kwargs):
        """
        Initialize the board using i2c
        :param gain: #   2/3 = +/-6.144V
                    #     1 = +/-4.096V
                    #     2 = +/-2.048V
                    #     4 = +/-1.024V
                    #     8 = +/-0.512V
                    #    16 = +/-0.256V
                    # See table 3 in the ADS1015/ADS1115 datasheet for more info on gain.
        :param usage: # -1 to -4 :  independant channel
                    #    0 = Channel 0 minus channel 1
                    #    1 = Channel 0 minus channel 3
                    #    2 = Channel 1 minus channel 3
                    #    3 = Channel 2 minus channel 3

        """
        # try :
        self.board=Adafruit_ADS1x15.ADS1115()
        self.err=0
        # except :
        #     self.board=0
        #     self.err=1

        if(isinstance(usage, int)):
            usage=[usage]
        if (isinstance(gain, int)):
            gain = [gain]
        self.setusage(usage)
        self.setgain(gain)

        super(class_analog_ADS1115, self).__init__(**kwargs)
        if len(usage)==len(gain):
            self.channel=len(usage)
        else :
            self.log.error("usage and gain don't have the same size")

    def read(self,channel,volts=True):
        """
        Read the value of the channel
        :param channel: the channel you want to read
        :param volts: T/F Format of the output (volts or bytes)
        :return: value
        """
        if channel<self.channel:
            usage=self.usage[channel]
            gain = self.gain[channel]
            if usage >= 0:
                val=self.board.read_adc_difference(usage, gain=gain)
            else :
                val=self.board.read_adc(abs(usage)+1,gain=gain)
            if volts:
                return self._tovolt(val,gain)
            else :
                return val
        else :
            self.log.error("Incorrect channel")
            return 0
    def setgain(self,gain):
        """
        Set the of each channel
        :param gain: List of gain by channel
        :return: self
        """
        self.gain=gain
    def setusage(self,usage):
        """
        Set the usage for each channel
        :param usage: List of usage

        :return: self
        """
        self.usage = usage
        # self.channel=len(self.usage)

        return self
    def _tovolt(self,byte,gain):
        range=4.096 #range for gain=1
        range=range/gain
        return byte*(range/32767)


class class_analog_bool(class_analog_ADS1115):
    def __init__(self, board,**kwargs):

        self.type = "analog"
        self.board = board
        if self.board == '1115':
            self.analog=class_analog_ADS1115(**kwargs)
            self.err = self.analog.err
        else :
            self.analog=None
            print("Error the analog board is unknown")
            self.err=1


    def state(self):
        return self.analog.state(channel=0)



class class_external:
    """
    A class for external invent (such as movie player ...)
    """
    def __init__(self,debug):
        self.err=0
        self.type='external'
        self.state=False
        self.debug=debug

    def state(self):
        return self.state

    def change_state(self,state):
        self.state=state
        return self
