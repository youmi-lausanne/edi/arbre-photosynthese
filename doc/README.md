2018 EDI Photosynthese Mandat Externe	
=================
Table of Contents

1. [Inputs/Outputs](#inputsoutputs)
    1. [Inputs :](#inputs-)
    2. [Outputs :](#outputs-)
    3. [Données Techniques :](#données-techniques-)
2. [Principe](#principe)
    1. [En Bref](#en-bref)
    2. [En Détails](#en-détails)
3. [PCB :](#pcb-)
    1. [Board principale](#board-principale)
        1. [Inputs](#inputs)
        2. [Outputs](#outputs)
    2. [Controle Brumisateur](#controle-brumisateur)
4. [Fichiers sources :](#fichiers-sources-)
        1. [btnmng.py](#btnmngpy)
        2. [dispmng.py](#dispmngpy)
        3. [gen_inifile.py](#gen_inifilepy)
        4. [inputmng.py](#inputmngpy)
        5. [MNG_PARBRE.py](#mng_parbrepy)
        6. [motormng.py](#motormngpy)
        7. [movie.yml](#movieyml)
        8. [outmng.py](#outmngpy)
        9. [run.py](#runpy)
        10. [setup.py](#setuppy)
        11. [testanalog.py](#testanalogpy)
        12. [testmotor.py](#testmotorpy)
        13. [timersmng.py](#timersmngpy)
5. [Videos](#videos)
    1. [ACT_E\$x-V\$x-S\$x.mp4 :](#act_ex-vx-sxmp4-)
    2. [standby.mp4](#standbymp4)
    3. [Fichiers de l'edi pour les vidéos :](#fichiers-de-ledi-pour-les-vidéos-)
6. [Fichiers de configuration](#fichiers-de-configuration)
    1. [movie.yml](#movieyml)
    2. [MNG_PARBRE.py](#mng_parbrepy)
        1. [Regler les inputs](#regler-les-inputs)
        2. [Regler les outputs](#regler-les-outputs)
        3. [Regler les timers](#regler-les-timers)
    3. [Tips](#tips)
        1. [Changer les pulses sur le brumisateur](#changer-les-pulses-sur-le-brumisateur)
        2. [Changer la vitesse du step motor](#changer-la-vitesse-du-step-motor)
        3. [Changer les timers pour la vidéo finale](#changer-les-timers-pour-la-vidéo-finale)
7. [Technique](#technique)
    1. [Alimentation](#alimentation)
    2. [Transistor](#transistor)
    3. [Moteur à pas](#moteur-à-pas)
        1. [Driver TB6600](#driver-tb6600)
    4. [Brumisateur](#brumisateur)
        1. [Commande](#commande)
        2. [Relais 220V](#relais-220v)
        3. [Optocoupleur](#optocoupleur)
        4. [Platine relais, opto](#platine-relais-opto)
    5. [Anémomètre](#anémomètre)
        1. [Connections :](#connections-)
    6. [Sonde de debit eau](#sonde-de-debit-eau)
    7. [Ecran](#ecran)


Explique la photosynthèse : CO2, Eau, Lumière= Sucre = Production de bois

---

# Inputs/Outputs

## Inputs :
* Le CO2 est détecté par une hélice connectée à un moteur (l’enfant souffle dans un tube)
* La lumière est connecté à un bouton poussoir 
* L’eau est amené par une pompe à eau détecté par une jauge de pression 

## Outputs :
* La lumiere s'allume
* Un ventilateur simule l'air
* L’eau est aspergé par un brumisateur 
* Un moteur pas à pas tourne

## Données Techniques :
*  Lorsqu'une action à été faite, on **ignore** si rejoué
*  Lorsqu'une action est faites pendant la lecture d'une vidéo, on **ignore ** l'action jusqu'à ce que l'on soit dans la partie *attente* de la vidéo (timer)
*  La lumiére s'allume imédiatement lors de l'appye sur le bouton
*  L'écran d'accueil est une vidéo qui tourne en boucle
*  Si il n'y a pas d'action jusqu'à un certain delais on retourne à l'écran d'accueil idem à la fin de le scene finale, lors de ce retour toutes les entrées/sorties sont remises à zero



# Principe 

## En Bref 


Chaque input génère une vidéo qui est ensuite mémorisé (vidéo d’attente)  jusqu’à avoir les 3 inputs : vidéos résultat.
Lors de la vidéo finale  le bruminisateur, le ventillateurse mettent en route et le moteur pas à pas fait un tour.
Un écran de veille est une vidéo qui tourne en permanence tant qu'il n'y a pas eu d'action


## En Détails

1. Demarage:
 
 1.1. L'application démare en lisant les fichiers vidéos (def dans MNG_PARBRE.py)

 1.2. Les inputs/output sont initialisés (def dans MNG_PARBRE.py)

 1.3. Le moteur est remis à zero
2. Fonctionnement :

 2.1. La vidéo standby.mp4 est lu en boucl tant au'un input n'est pas actionné, toutes les output sont DOWN

 2.2. Si un input est pressé :

  2.2.1. La vidéo correspondante à l'état de la machine est choisie

  2.2.2. Tant que le délai d'attente de la vidéo n'est pas atteind les inputs sont ignorés (def dans movie.yml)

  2.2.3. Si il n'y a pas d'action à la fin de la vidéo, on retour en 2.1.

  2.2.4. Si il y un input après l'attente et avant la fin, l'etat de la machine change et on retourne en 2.2.

  2.2.5. Si l'input est le bouton soleil, l'output LED est UP

  2.2.6 Si plus d'un input sont UP, une seule est chosie, il y a de toute maniére une sécurité qui empeche de lire un état qui n'a pas de vidéo correspondante

 2.3. Si l'état de la machine correspondant à une vidéo ne nécéssitant plus d'input (toutes les actions sont faites) :

 2.3.1. La vidéo finale est lue 

 2.3.2. Un ensembe de timers permettent de mettre UP et DOWN les diverses ouput : (def dans MNG_PARBRE.py)

* FAN (ON/OFF output)
* Brumisateur 220 (ON/OFF output)
* Brumisateur control (pulses)
* Step motor (motor)
 
  2.3.3. A la fin de la vidéo finale on retourne en 2.1.

---


# PCB :
## Board principale
![IMG_2862](./media/1669935245.jpeg)


### Inputs

| Input |   Connected to|  Rasp PIN |  |
|---|---|---|---|
| IN 1 | Sonde 1 | 4 |  |
| IN 2 |  | 17 |  |
| IN 3 | BTN LUM | 18 |  |
| IN 4 | Motor zero | 27 |  |
| I2c | Analog board | I2c Pin | In 0 et 1 utilisé pour anénometre | 

### Outputs

* Les outputs sont en 12V avec switch sur le ground.
* 1 et 6 sont les 12V +

| Output |   Connected to|  Rasp PIN |  |
|---|---|---|---|
| OUT 1 (2)| lampe LED | 6 | FP |
| OUT 2 (3)| ventillo | 5 | FP |
| OUT 3 (4)| brum 220 | 12 | P |
| OUT 4 (5)| brum ctl | 13 | P |

* _ P_ : Faible puissance 0.5A max
* _FP_ : Forte puissance  : 3A

|  Out SM  |  stepper motor |  |  | 
|---|----|---|---|
| Ena +| 14| | | 
| PUL + | 20| | |
| DIR + | 19| | |

* Alim Max 6.7 A @ 12V

## Controle Brumisateur
![IMG_2864](./media/1498037603.jpeg)

* Le relais 220 ouvre ou ferme le circuit 220V du brumisateur
* L'optocoupleur est utilisé pour mimer le bouton poussoire de control (pulse pour demarer n= ?)

---

#  Fichiers sources :

### btnmng.py
Classes pour le management des inputs boutons
### dispmng.py
Classes pour le display 
### gen_inifile.py
Génére le fichier de configuration movie.yml
### inputmng.py
Classes pour la gestion des inputs
### MNG_PARBRE.py
Classes adapatés pour le montage ainsi que de la configuration
### motormng.py
Classes pour le management de moteur
### movie.yml
Fichier de configuration des vidéos
### outmng.py
Classses pour le management d'output
### run.py
Fichier Principal du montage
### setup.py
Fichier de configuration (modules nécessaires)
### testanalog.py
Fichier test pour entrée analogiaue
### testmotor.py
Fichier test pour le step motor
### timersmng.py
Classes pour la gestion des timers


# Videos
Les videos sont dfans le dossier media, elles sont chargés en fonction des fichiers de configuration (voir plus bas)

## ACT_E\$x-V\$x-S\$x.mp4 : 
* $x pouvant prendre 0,1,2 => Non fait, En cours, Fait
* ( 0 0 0 n'existe pas)
* Par exemple 1-2-0 signifie Eau en cours, Vent fait et Soleil non fait
* Toutes les vidéos contenant deux 2 et un 1 (par exemple 2-1-2) conduisent à la scene finale
* Attention à la casse des fichiers 

*  **2 2 2 est la scène finale** 

## standby.mp4 
 vidéo de l'écran d'accueil qui tourne en boucle



#  Fichiers de configuration

## movie.yml
* Définit le temps de non-action (pendant la lecture) pour chaque fichier video
* Format : 
	* fichier*: {no_act_wait:*temps-en-s* }
	* Exemple : ACT_E0-V0-S1.mp4: {no_act_wait: 10}
	* Le script *gen_inifile.py* genere un fichier conf par défaut

## MNG_PARBRE.py

class pcb_Parbre contient la configuration de :

* Input bouton avec la variable *btn* 
	* Du regroupement des boutons avec la variable *grp*
	* Des outputs avec *out*
	* Des timers avec *timers_channel*
	* Les reglages de taille d'écran ne sont pas implémentés pour la vidéo, les videos seront forcement lue en plein écran 

### Regler les inputs

* useled : can turn on/off an output when the button is pressed
	 * si on utilise useled : 'useled' : True,'setled':{'pin' : 12} 
	* pin : the rpi pin in GPIO mode

* type : *btn*
	 * Ce type est pour les boutons ON/OFF
	 * {'type':'btn','set':{'pin' : 4, 'pudup':True,'trig':'falling'},  'useled' : False}
	 * pin : the rpi pin in GPIO mode
	 * pudup : push down or push up
	 * trig : defenie le trigger peut être *falling* or *raising* 
 
* type : *analog*
	* Convertit une entrée analogique en binaire avec un threshold parametré
	 * {'type': 'analog','set':{'board':'1115','usage' : 0,'gain':16,'threshold':{'value':100,'unit':'B','abs':True}}, 'useled': False,},
	 * board : board utilisée
	 * usage : definit comme sont utiliseés les inputs (voir inputmng.py)
	 * gain : amplificateur sur la board  (voir inputmng.py)
	 * threshold : definit quand le trigger est UP : 
	  * value : valeur analogique
	  * unit peut etre *B* bytes ou *V* volts (experimental)
	  * abs : prends la valeur absolue ou non 

### Regler les outputs 

* type : *out* (outmng.py)
	*  Type ON/OFF
	*  {'type': 'out', 'args':{'pin': 13,}}
 * pin : the rpi pin in GPIO mode

* type : *pulses* (outmng.py)
 	* genere un train de n pulses de longueur parametre  
	 * 'type': 'pulses', 'args': {'pin': 6,'npulse': 4, 't_on': 0.2, 't_off': 0.2}}
	 * pin : the rpi pin in GPIO mode
	 * npulse : le nombre de pulses a generer
	 * t_on temps de UP (en s)
	 * t_off temps en down (en s)

* type : *smotor*(motormng.py)
 	*  Pour le control du step motor 
	 * 'type': {'smotor', 'args': {'pinENA': 14, 'pinDIR': 19, 'pinPUL': 20, 'rev': 6400, 
	'homing': True, 'homeset': {'type': 'btn','set': {'pin': 27, 'pudup': True,'trig': 'falling'},
	'useled': False }
	* args :
	 * pinENA pinDIR pinPUL : defiinit les pin rpi en GPIO mode
	 * rev : nombre d'impulsion pour faire un tour
	 * homing T/F : y a til un bouton home
	 	* Si oui, homeset et de type inputs (voir au dessus) 


### Regler les timers
Les timers prennent un temps en input (ici le temps de la video) est declenche *start* quand le temps est atteind, un évenement stop est déclenché aprés un certains temps ou apréès un nombre de fois
*{'out_use': 0,
 'start': {'source': 'external', 'source_inp': 0, 'from': 'start', 't': 10, 'use': {'fct': 'start', 'args': {}}},
 'stop': {'source': 'external', 'source_inp': 0, 'from': 'end', 't': 30, 'use': {'fct': 'stop', 'args': {}}}},

* out_use : position dans le tableau out à utiliser
* start definit l'évenement start 
	 * source : external (le temps est définit en dehors de la classe le seul implementé pour le moment)
	 * source_inp : quel timer on veut utiliser
	 * from définit si le temps en input correspond au début (*start*) ou à la fin (*end*) si on utilise end le temps total doit etre définit quelque part (en utilisant *set_length()*)
	 * t definit a quel moment trigger
	 * use : quel fct de la classe utiliser et les args en input

* stop ;
	 * Evenement stop ressemble a stop si le stop est temporel, possibilité de faire un start *n* fois et de finir avec un stop :
	 *   'stop': {'source': 'ntimes', 'source_inp': 1, 'use': {'fct': '', 'args': {}}}}
	 * source_inp : le nombre de fois qu'on execute start


## Tips

### Changer les pulses sur le brumisateur

* Le reglage à changer dans  MNG_PARBRE.py class pcb_Parbre:
* out[2] definit le train de pulse : npulse est le nombre de pulse t_on et t_off definssent la vitesse des pulses en seconde

### Changer la vitesse du step motor

*  Le reglage à changer dans  MNG_PARBRE.py class pcb_Parbre:
* timers_channel[3] contient les args de la fonction start le parametre *speed* definit la vitesse du moteur en  seconde par tour
 
### Changer les timers pour la vidéo finale
*  Le reglage à changer dans  MNG_PARBRE.py class pcb_Parbre:
* timers_channel contient les réglages des timer, from start est le début de la video, from end est depuis la fin de la video.
* en cas de stop ntimes la fonction start est appelle source_inp fois et ensuite stop()




---
# Technique


## Alimentation 

 GST90A12-P1M 12 V/DC 6.67 A 80 W


## Transistor
* P : BC547C : http://www.farnell.com/datasheets/59764.pdf
* FP : BD241CTU :https://www.mouser.ch/datasheet/2/308/BD241C-1305176.pd

## Moteur à pas 

![media-32121](./media/32121.jpeg)

* Il s'agit d'un moteur Qmot QSH5718-76-28-189 [QSH5718_manual.pdf](./media/29941.pdf)
, il fait des pas de 1.8 ° et à un courant de 2.8A, la resistance de coil est de 1.13 ohms


* [Adafruit all about stepper motors](https://learn.adafruit.com/all-about-stepper-motors)
* [https://www.adafruit.com/product/2448](https://www.adafruit.com/product/2448) ?

### Driver TB6600

* Cable vers Rpi 
	* Noir : GND (DIR - et PUL-)
	*  Blanc : Dir +
	* Rouge : PUL+
	*  Vert : ENA +

* Rpi
	* Ena +: 14
	* PUL + : 20
	* DIR + : 19


* Moteur :
	* B- Bleu
	* B+ Rouge
	* A- Vert
	* A+ Noir

* Le moteur doit faire 6400 pas pour faire un tour
* Le switch est connecté sur la 4eme input contact ferme = le swith est ouvert au zero



* Manuel : [TB6600 User Guide V1.2(1).pdf](./media/12869.pdf)

* https://www.raspberrypi.org/forums/viewtopic.php?t=186016
* https://www.banggood.com/3xTB6600-Upgraded-Version-32-Segments-4A-40V-5786-Stepper-Motor-Driver-p-1145353.html?rmmds=myorder
* http://www.haoyuelectronics.com/Attachment/TB6600_module/TB6600_Datasheet.pdf





## Brumisateur
Un relais 220V allume le brumisateur et il faut ensuite envoyer ... impulsion sur la commande pour démarer la fumée.
On ouvre le relais pour eteindre
### Commande 

### Relais 220V
![media-WLxYVX](./media/25209.png)

[Datasheet S4-12V.pdf](./media/2186.pdf)

### Optocoupleur 
* KB827
* https://www.promelec.ru/pdf/KB827-M.pdf

### Platine relais, opto


Bornier bleu : relais 220
Bornier ver, sortie optocouleur 

12V + Commun et - pour relais et optocoupleur
Cable 
*  Rouge : +12V
* Vert : Opto -
* Blanc : relais -
*  Noir : NC



## Anémomètre 
plus le moteur tourne vite, plus l'intensité augmente. la fréquence reste la même (env. 61 Hz)
En soufflant à la bouche, on arrive env. à +-14mV d'amplitude max


ADS1x15 VDD to Raspberry Pi 3.3V
ADS1x15 GND to Raspberry Pi GND
ADS1x15 SCL to Raspberry Pi SCL
ADS1x15 SDA to Raspberry Pi SDA

### Connections :

* Anénometre flottant sur A0 et A1
- Rouge VDD 3.3V
- Noir GND
- Vert SCL
- Blanc SDA



* [https://learn.adafruit.com/raspberry-pi-analog-to-digital-converters/ads1015-slash-ads1115](https://learn.adafruit.com/raspberry-pi-analog-to-digital-converters/ads1015-slash-ads1115)
* <https://cdn-shop.adafruit.com/datasheets/ads1115.pdf>

* Python lib : https://github.com/adafruit/Adafruit_Python_ADS1x15


## Sonde de debit eau 

* VDC : de 5 à 24V fonctionne aussi avec  le 3.3V du raspberry
* Output (Hz) NPN Sinking Open Collector @ 20mA Maximum Leakage Current 10μA (Pull-Up Resistor Required)

* [https://raspberrypi.stackexchange.com/questions/34480/how-to-use-the-water-flow-sensor-with-raspberry](https://raspberrypi.stackexchange.com/questions/34480/how-to-use-the-water-flow-sensor-with-raspberry)

* [https://raspberrypi.stackexchange.com/questions/62300/flow-meter-raspberry-circuit-and-problem-receiving-pulses](https://raspberrypi.stackexchange.com/questions/62300/flow-meter-raspberry-circuit-and-problem-receiving-pulses)

* [Capteur de débit* ](./media/s/29522.pdf)


## Ecran
*Philips 32p 1920x1080 haut parleur intégré*

[https://www.displayspecifications.com/fr/model/335769a](https://www.displayspecifications.com/fr/model/335769a)

* Diag 801mm
* Largeur 700
* Hauteur 395 mm

* Avec cadre :
	* Largeur 730
	* Hauteur 425



###Images de l'écran

![media-ofQjvb](./media/26996.jpeg)

![media-PXWQUV](./media/4804.jpeg)

![media-yHVZpL](./31616.jpeg)
