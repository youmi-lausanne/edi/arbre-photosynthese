import pygame
import os
from pathlib import Path
import numpy as np
import math
import reusables
import logging
from moviepy.editor import *
from omxplayer.player import OMXPlayer
"""
Dispmng is the root class for display management it usualy used by another class specific of the setup
it contains generic functions for interacting with the display (through pygame)



"""




class dispmng:
    cw=(255, 255, 255)
    cb=(0, 0, 0)
    cr=(255,0.0)
    cg=(0,255,0)
    offset_x=50
    offset_y=10
    def __init__(self,debug=False,ratio=2):
        self.log = reusables.get_logger('dispmng', level=logging.DEBUG)
        self.debug=debug

        #Pygame Init :
        pygame.init()
        pygame.display.init()
        if not self.pctest : # for raspberry display
            self.screen=pygame.display.set_mode((0,0),pygame.FULLSCREEN )
            self.adjimg=False # We dont want the images to be adjusted of the screen resolution
            pygame.mouse.set_visible(False)
            font_file="/usr/share/fonts/truetype/freefont/FreeMono.ttf" # font use by raspberry
            self.ratio=1

        else : #For pc display (testing)
            self.ratio=ratio # Ratio divid the screen resolution for windows
            sw=int(1920/self.ratio)
            sh=int(1080/self.ratio)
            self.screen=screen=pygame.display.set_mode([sw,sh])
            self.adjimg=True
            font_file=pygame.font.get_default_font()

        self._defscreensize()

        #text def
        self.size_tlt = 64 # Font size for title
        self.size_txt = 32 # Font size for txt
        self.space=20 # Line space
        #Fonts Init :
        self.ftlt = pygame.font.Font(font_file, self.size_tlt)
        self.ftxt = pygame.font.Font(font_file, self.size_txt)
        # clean screen and display loading screen
        self.clean_screen()
        self.loading()
        self.didfade=False
        self.movie=movieplayer("")


    def _defscreensize(self):
        """
        setup all the variables for screen size
        :return: self
        """
        if self.pcb.useFS:
            xy0=(0,0)
            xy1=self.screen.get_size()
            xyc=(int(self.screen.get_width()/2),int(self.screen.get_height()/2))
            sz=xy1
        else :
            xyc=self.pcb.screen_center
            ss=self.pcb.screen_size

            sl=ss[0]/self.ratio
            sw=ss[1]/self.ratio
            xy0=(int(xyc[0]/self.ratio-sl/2),int(xyc[1]/self.ratio-sw/2))
            xy1=(int(xyc[0]/self.ratio+sl/2),int(xyc[1]/self.ratio+sw/2))
            sz=(sl,sw)
        self.xy0=xy0 #Upper/Right conner of the screen
        self.xy1=xy1 #Lower/Left conner of the screen
        self.xyc=xyc #Center of the screen
        self.sz=sz # Screen size
        self.offset_x+=xy0[0] #X offset (for text)
        self.offset_y+=xy0[1] #Y offset (for text)
        self.x=xy0[0] # Initial position x
        self.y=xy0[1] #Initial position y
        return self

    def loading(self):
        """
        Make a loading screen
        :return: self
        """
        self.make_title("Loading... \n Please wait",True)
        return self

    def pnotworking(self):
        """
        Make a not working screen
        :return: self
        """
        self.make_title("Not working",True)
        return self

    def pready(self):
        """
        Make a ready screen
        :return: self
        """
        self.make_title("Ready :) \n Starting in 5s...",True)
        return self


    def clean_screen(self,prt=True):
        self.x=self.offset_x
        self.y=self.offset_y
        self.txt=""
        self.title=""
        self.screen.fill(self.cb)
        self.make_print()
        return self

    def doquit(self):
        """
        return true if quit event detected
        :return:  T/F
        """
        doquit=False
        for event in pygame.event.get():
            if event.type == pygame.QUIT: # This would be a quit event.
                doquit = True # So the user can close the program
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                doquit = True
        return doquit


    def _ptext(self,font,txt,x,y,tcolor):
        """
        Internal function for generating text
        :param font: font used
        :param txt: text
        :param x: position x
        :param y: position y
        :param tcolor: text color
        :return: pygame text object
        """

        ftxt=font.render(txt, True, tcolor)
        self.screen.blit(ftxt,(x,y))
        return ftxt.get_height()

    def length_text(self,txt):
        """
        Return length of a text
        :param txt: the input text
        :return: length of the text in pixel
        """
        ftxt=self.ftlt.render(txt, True,self.cw)
        return ftxt.get_width()


    def make_title(self,txt,prt=False):
        """
        Make a title screen and print it (if prt=True)
        :param txt: the text of the title
        :param prt: T/F make the print on the screen
        :return: self
        """
        self.screen.fill(self.cb)
        lines=txt.splitlines()
        self.y=self.offset_y
        for line in lines:
            self.y += self.space + self._ptext(self.ftlt,line,self.offset_x,self.y,self.cw)
        self.title=txt
        if prt :
            self.make_print()
        return self

    def add_txt(self,txt,prt=True,color=cw):
        """
        Add txt and print it
        :param txt:  can be multiline (split with \n)
        :param prt: Make the print (T/F)
        :param color: color of the text
        :return: self
        """
        lines=txt.splitlines()
        for line in lines:
            self.y += self.space + self._ptext(self.ftxt,line,self.x,self.y,color)

        self.txt +=txt
        if prt :
            self.make_print()
        return self

    def make_print(self):
        """
        Print the surface
        :return: self
        """

        if not self.pcb.useFS:
            self.make_maskrect()


        pygame.display.flip()

        return self

    def make_maskrect(self,color=cb):
        """
        Make a mask when all the screen is not used (use_FS=False)
        :param color: color of the mask
        :return: self
        """
        ss=self.screen.get_size()
        l1=self.xy0[0]
        w1=ss[1]
        l2=self.sz[0]
        w2=self.xy0[1]
        l3=ss[0]-(l1+l2)
        w4=ss[1]-self.xy1[1]
        rects=[pygame.Rect(0,0,l1,w1),pygame.Rect(self.xy0[0],0,l2,w2),pygame.Rect(self.xy1[0],0,l3,w1),pygame.Rect(self.xy0[0],self.xy1[1],l2,w4)]
        for i,r in enumerate(rects):
           pygame.draw.rect(self.screen,color,r)
        return self

    def dispmedia(self,media,prt=True):
        """
        Display the given media
        :param media: input media
        :param prt: if True the media is immediatly print
        :return: self
        """
        if media['type']=='img':
            self.dispimg(media['media'],prt)
            self.movie = movieplayer("")

        elif media['type']=='movie' :
            self.movie=media['media']
            if prt :
                self.movie.play()
        else :
            self.log.error("Cannot display the media type unknown")
            self.movie = movieplayer("")

            return -1
        return self

# This series control the movie player by making a link with self.movie created by dispmedia
    def movie_isplaying(self):
        return self.movie.is_playing()

    def movie_ispaused(self):
        return self.movie.is_paused()

    def movie_play(self):
        self.movie.play()
        return self

    def movie_pause(self):
        self.movie.pause()
        return self

    def movie_autoquit(self):
        self.movie.autoquit()
        return self
    def movie_quit(self):
        self.movie.quit()
        return self

    def dispimg(self,img,prt=True):
        """
        Display an image using pygame
        :param img: pygame img
        :param prt: immediatly printed or put into memory
        :return: self
        """
        self.screen.fill(self.cb)
        pos=self.xy0
        self.screen.blit(img,pos)
        if prt:
            self.make_print()
        return self
    def clean_txt(self,prt=True):
        """
        Clean only the txt and leave the title
        :param prt: immediatly printed or put into memory
        :return: self
        """
        title=self.title
        self.clean_screen()
        self.make_title(title,prt)
        return self

    def get_imgscale(self,img):
        """
        return the image size and take care of scaling (if enabled)
        :param img: pygame img
        :return: (x_size,y_size)
        """
        (xs,ys)=self.sz
        (xi,yi)=img.get_size()
        if xi>yi :
            ratio=xs/xi
        else :
            ratio=ys/yi
        nxs=int(xi*ratio)
        nys=int(yi*ratio)
        return nxs,nys


    def loadmedia(self,filename,alpha=True):
        """
        Load media into memory
        :param filename: location of the media (path)
        :param alpha: for img uses alpha
        :return: media{media,type} , err
        """
        path=Path(filename)
        ext=path.suffix
        ext.lower()
        if ext in reusables.exts.pictures :
            typ='img'
            (media,err)=self.loadimg(filename,alpha)
            out = {'type': typ, 'media': media}
        elif ext in reusables.exts.video :
            typ='movie'
            try :
                (media,err)=self.loadmovie(filename)
                vc=VideoFileClip(filename)
                out = {'type': typ, 'media': media, 'duration': vc.duration, 'videoclip': vc}
            except :
                err = 1
                out = {'type': typ, 'media': media, 'duration': -1, 'videoclip': -1}

        else :
            self.log.error("File : " + filename + " format unknown")
            err=1
            media=[]
            typ='none'
            out = {'type': typ, 'media': media}

        return out,err
    def loadmovie(self,filename):
        """
        Load movie into memory
        :param filename: path of the movie
        :return: mov,err
        """
        if os.path.isfile(filename):
            mov=movieplayer(Path(filename))
            err = 0
        else:
            self.log.error("File : " + filename + " not found")
            err = 1
            mov = []

        return mov, err

    def loadimg(self,filename,alpha=True):
        """
        return image in pygame surface (from disk)
        :param filename: path's img
        :param alpha: use alpha (for transparence)
        :return: img , err
        """
        if os.path.isfile(filename):
            img =pygame.image.load(filename)
            if alpha:
                img.convert_alpha()
            else :
                img.convert()
            if  self.adjimg:
                img=pygame.transform.smoothscale(img,self.get_imgscale(img))
            err=0
        else :
            self.log.error("File : " +filename+" not found")
            err=1
            img=[]
        return img,err
    def run_fct(self):
        """
        Return true if quit or escape touch detected usage for function
        :return: T/F
        """
        quit_fct=False
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit_fct = True
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                quit_fct = True
            if event.type == pygame.KEYDOWN and event.key == pygame.K_m:
                quit_fct = True
        return quit_fct

    def fadescreen(self,t,rate=3,order=0,color=cb):
        """
        Make the fade screen for fade effect (to or from color) depending of t
        :param t: the actual t
        :param rate:  the rate of the color per second
        :param order: from or to the color
        :param color: the color of the fade
        :return: self
        """
        if not self.didfade:
            self.alphaSurface = pygame.Surface(self.sz)
            self.alphaSurface.fill((color))
            self.didfade=True
        if order==0:#Fondue au noir
            alph=t/rate*255
        else :
            alph=255-t/rate*255
        if alph>255:
            alph=255
        if alph<0:
            alph=0
        self.alphaSurface.set_alpha(alph)
        pos=self.xy0
        self.screen.blit(self.alphaSurface,pos)
        return self

    def diaporama(self,tin,diap):
        """
        Make a diaporama of pygame pics
        :param tin: time
        :param diap: array of pygame img
        :return: self
        """
        t = tin
        n=len(diap.imgs)
        ti=[]
        for i in range(n):
            ti.append(diap.imgwait[i]+2*diap.blackrate+diap.blackwait)

        ttot=sum(ti)
        t=t-ttot*math.floor(t / ttot) #convert total time in diaporama time (remove multiple loop)
        tsumi=np.cumsum(ti)
        posi=np.where((tsumi >= t) == True)[0][0] #give the diapositive position
        tposi=t
        if posi > 0:
              tposi -=tsumi[posi-1] #time in the particular slide
        a = diap.imgwait[posi]
        b = a + diap.blackrate
        c = b + diap.blackwait

        #d = c + diap.blackrate
        #if scr=0 : display the i_img
        #if scr=1 display a black screen
        # if scr=2 display the i+1 img

        #order = 0 : img to black
        #order = 1 : black to img
        order=0
        scr=0
        tf=0
        if tposi > c:
            scr = 2
            fondue = True
            order = 1
            tf = tposi - c
        elif tposi > b:
            scr = 1
            fondue = False
        elif tposi > a:
            fondue = True
            tf = tposi - a
        else:
            scr = 0
            fondue = False


        if (self.debug):
            self.log.debug("posi=",posi)
            self.log.debug("tposi=", tposi)
            self.log.debug("scr=", scr)
            self.log.debug("t=", t)
            self.log.debug("tsumi=",tsumi)
        if scr == 0: #display the i_img
            self.dispimg(diap.imgs[posi], prt=False)
        elif scr == 1: # Black screen
            self.screen.fill(self.cb)
        elif scr == 2: #display the i+1_img
            posi += 1
            if posi >= n:
                posi = 0
            self.dispimg(diap.imgs[posi], prt=False)
        if fondue:
            self.fadescreen(tf, rate=diap.blackrate, order=order)

        self.make_print()
        return self



class movieplayer:
    """
    Add the capacity to play movie with omxplayer (only for raspberry pi)
    TODO : Include the possibility of non full screen
    """

    def __init__(self,filepath):
        """
        Init the movie player
        :param filepath: path of the movie
        """
        self.path=filepath
        self.played=False
        self.paused=False
        self.loop=False
        self.player=[]
    def play(self,loop=False):
        """
        Play the movie
        :param loop: Play in loop
        :return: self
        """
        if not self.paused:
            args = ['--no-osd', '--no-keys', '-b']
            if loop:
                self.loop=True
                args.append('--loop')
            else :
                self.loop=False
            self.player = OMXPlayer(self.path,args=args)
            self.paused=False
        else :
            self.player.play()
            self.paused=False
        return self

    def is_playing(self):
        """
        State of the player
        :return: True if playing
        """
        try:
            self.player.playback_status()
            self.played = True
        except:
            self.played = False
        return self.played

    def pause(self):
        """
        Pause the player
        :return: self
        """
        if self.is_playing():
            self.player.pause()
            self.paused=True
        return self

    def quit(self):
        """
        Quit the player by destroying the connector
        :return: self
        """
        self.played=False
        self.paused=False
        self.player.quit()
        return self

    def is_paused(self):
        """
        Return True if player is paused
        :return: T/F
        """
        return self.paused

    def autoquit(self):
        """
        Auto quit the player if the movie is finished
        :return: self
        """
        if self.played and not self.is_playing():
            self.quit()
        return self