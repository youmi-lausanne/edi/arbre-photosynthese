import RPi.GPIO as GPIO
from pirc522 import RFID

import reusables
import logging
from outmng import *
from inputmng import *

GPIO.setmode(GPIO.BCM)# #GPIO Init TODO by removing rfid GPIO not needed

"""
This module manages everything about boutons and led

It's divided into different classes :

----class_mngbtn----
This is the master class it manages all the other classes for a full integration in the main soft
In contains fct for detecting btn pressed (or changed) managing soluce btn, rfid into simple fct
It manages correct answer(s) (and bad(s))
It also mananging reset (for waiting animation for example)
It find most of the seting in the pcb variable

----class_grpbtn----
This is class groups btn together for group action
one element of a grp of class_btn
In contains fct for detecting btn pressed and can turn on/off led

If select=True only one btn in the group can be on and the lastest is memorised (selected)
                in the case of useled=True the led of the selected is turn ON (others are OFF)

if select=False action occurs when one btn is pressed or released, there is not selection
    If no btn are pressed -1 is return has value, first btn is [0]

----class_btn----
This is the simplest class it manages the btn and led actions
one element of the class is ONE btn/led
In contains fct for detecting btn pressed and can turn on/off led
A btn is composed of a pinbtn (in GPIO mode)
If useled = True a pinled need to be provided


----rfid_card (class of class_grpbtn)----
Management of the rfid reader simulates a group of btns by replacing some fct of grb_btn to interact with rfid card
Each rfid card is like a btn (with a number) when a card is detected a corresponding number is given

"""



class class_mngbtn:

    """
    ----class_mngbtn----
    This is the master class it manages all the other classes for a full integration in the main soft
    In contains fct for detecting btn pressed (or changed) managing soluce btn, rfid into simple fct
    It manages correct answer(s) (and bad(s))
    It also mananging reset (for waiting animation for example)
    It find most of the seting in the pcb variable
    """
    def __init__(self,pcb,debug=False,thome=20,tsleep=0.1):
        """
        Initialize the management btn class
        :param pcb: class of setting for the setup
        :param debug: T/F Extra display
        :param thome: Time (in s) for going back to home screen
        :param tsleep: Time (in s) for wainting loop
        """
        self.log = reusables.get_logger('mngbtn', level=logging.DEBUG)

        self.pcb=pcb #pcb is the "settings" variable
        self.n=len(self.pcb.grp) #number of group

        self.debug=debug#Gives extra info in terminal
        self.gbtn=[]
        self.changed=False #something changed
        self.nreset=False #need reset
        self.ansreset=False #did a reset occured
        self.tla=time.time()-thome #timing fct
        self.randomt=time.time()
        self.thome=thome #when do we have to reset (going home screen) in s
        self.err=0 #error 0 if not
        self.correct=self.pcb.correct #correct answers
        self.tsleep=tsleep

        #MNG INIT :

        #Init grp btn :
        for g in range(self.n):
            self.gbtn.append(class_grpbtn(self.pcb.grp[g],pcb.btn,self.debug,select=not self.pcb.updateoff))
            self.err += self.gbtn[g].err
        #Init Sol btn :
        if pcb.usesolbtn :
            self.sol=True
            self.solbtn=class_grpbtn([self.pcb.solbtn],pcb.btn,debug=self.debug,select=False)
        else :
            self.sol=False
            self.solbtn=0

        #Init RFID
        if self.pcb.rfid:
            if self.debug:
                self.log.debug("add RFID")
            self.gbtn.append(rfid_card(cardvsbtn=self.pcb.rfid_card,select=not self.pcb.rfidupdateoff,debug=False)) # add the rfid like a group of btn
            self.n+=1 #we add rfid in the total nbr of btn
            self.err +=self.gbtn[-1].err

    def ispressed(self,sol=True):
        """
        Return a list of pressed grp (bool)
        :param sol: add soluce btn
        :return: List of booleans
        """
        self.checkbtn(sol=sol)
        k=[]
        for i,p in enumerate(self.pressed):
            if p>-1 :
                ans=True
            else:
                ans=False
            k.append(ans)
        return k
    def list_pressed(self,sol=False):
        """
        Return a list of pressed grp (btn id)
        :param sol: add soluce btn (T/F)
        :return: List of btn id
        """
        self.checkbtn(sol=sol)
        return self.pressed

    def is_solpressed(self):
        """
        Return True is solution btn is pressed
        :return: T/F
        """
        if self.sol:
            state= self.solbtn.ispressed()
        else :
            state=False
        if state and self.debug:
            self.log.debug("sol btn is pressed")
        return state

    def solledon(self):
        """
        Turn on the led of the soluce btn
        :return: self
        """
        if self.sol:
            self.solbtn.allledon()
        return self
    def isallpressed(self,sol=False):
        """
        Return True if all the grp are pressed
        :param sol: count soluce btn too ? (T/F)
        :return: Boolean True if all the grp are pressed
        """
        l=self.ispressed(sol=sol)
        return all(l)

    def checkbtn(self,sol=True):
        """
        Check the all the groups and update self.pressed
        :param sol: T/F check the soluce btn ?
        :return: self
        """
        self.pressed=[]
        for i in range(self.n):
            self.pressed.append(self.gbtn[i].select)

        if self.sol and sol:
            self.pressed.append(self.is_solpressed())
        return self

    def allledon(self):
        """
        Turn all the led on
        :return: self
        """

        for i in range(self.n):
            if self.debug :
                self.log.debug("led grp :"+str(i)+"ON")
            self.gbtn[i].allledon()
        return self
    def reset_led(self):
        """
        Reset all the led
        :return: self
        """
        for i in range(self.n):
            if self.debug :
                self.log.debug("led grp :"+str(i)+"reset")
            self.gbtn[i].reset_led()
        return self

    def reset_select(self):
        """
        Reset all select btn
        :return: self
        """
        for i in range(self.n):
            if self.debug :
                self.log.debug("btn grp :"+str(i)+"reset")
            self.gbtn[i].reset_select()
        return self
    def reset(self): #
        """
        Reset led and select in all grp
        :return: self
        """
        if self.debug:
            self.log.debug("reset asked")
        self.reset_led()
        self.reset_select()
        return self

    def action_btn(self):
        """
        check if something changed

        :return: self
        """
        changed=[]
        for i in range(self.n):
            self.gbtn[i].action_btn()
            changed.append(self.gbtn[i].changed)
        self.changed=any(changed)
        return self

    def randomled(self,sol=False,uset=True,t=0,step=1): #
        """
        Turn on leds on each group led on the grp choosen randomely
        :param sol: include the soluce btn led (T/F)
        :param uset: use a timer for changing the led (T/F) if false the led will change for each call of this function
        :param t:  actual time (needed if uset=True)
        :param step: Time step for changing (needed if uset=True)
        :return: self
        """

        if t>self.randomt+step or not uset :
            for i in range(self.n):
                self.gbtn[i].randomled()
            if sol:
                self.solbtn.randomled()
            self.randomt=t
        return self
    def setexternal(self,grp,use,select):
        """
        Set the external setting for a specific group
        :param grp: grp to change
        :param use: T/F using external selection or not
        :param select:  Value of the external select
        :return: self
        """
        self.gbtn[grp].setexernal(use=use,select=select)
        return self
    def external_on(self,grp,select):
        """
        Turn on select on grp with select value
        :param grp: grp to change
        :param select: Value of the external select
        :return: self
        """
        self.setexternal(grp=grp,use=True,select=select)
        return self
    def external_off(self,grp):
        """
        Turn off the external select for a specific group
        :param grp:
        :return: self
        """
        self.setexternal(grp=grp,use=False,select=-1)
        return self
    def external_alloff(self):
        """
        Turn off all the external select
        :return: self
        """
        for i in range(self.n):
            self.external_off(i)
        return self

    def is_changed(self,sol=True):
        """
        Return true if an input changed
        :param sol: does it include solice btn (T/F)
        :return: T/F
        """
        self.action_btn()
        if sol and self.sol :
            self.solbtn.action_btn()
            sols=self.solbtn.changed
        else :
            sols=False
        if self.changed or sols:
            self.tla=time.time()
            if self.ansreset:
                self.turnon_selectled() #Reset leds when we were in home mode
            if self.debug :
                self.log.debug("something changed")
            return True
        else :
            return False

    def needreset(self):
        """
        Return true if reset is needed
        :return: T/F
        """
        a=self.nreset #reset if first is_reset
        b=self.sol and (self.solbtn.changed and not self.is_solpressed()) # reset after a sol btn event
        return a or b

    def last_change(self):
        """
        Give the last changes
        :return:  last change in s
        """
        return (time.time()-self.tla) #in s
    def is_reset(self): # Return true if it's time for reset
        t=self.last_change()
        if t>= self.thome:
            ans=True
            if not self.ansreset:
                self.nreset=True
                self.ansreset=True
                self.randomt=t
            else :
                self.nreset=False
        else :
            ans=False
            self.ansreset=False
            self.nreset=False
        return ans
    def is_correct(self):
        """
         Return True is answer (btns selection) is correct
        :return: T/F
        """
        ans=[]
        for i,c in enumerate(self.correct):
            ans.append(c==self.list_pressed(sol=False))
        return any(ans)
    def ncorrect(self):
        """
        Return which group[n] is correct
        :return: integer
        """
        ans=[]
        for i,c in enumerate(self.correct):
            ans.append(c==self.list_pressed(sol=False))
        try :
            n=ans.index(True)
        except :
            n=-1
        return n

    def correct_ledon(self,n=0): #
        """
        Turn on the led for the [n] correct
        :param n: the group we want to turn on
        :return: self
        """
        for i,c in enumerate(self.correct[n]):
            self.gbtn[i].change_led(c,True)
        return self

    def turnon_selectled(self): #
        """
        turn on select led for each groups
        :return: self
        """
        for i in range(self.n):
            self.gbtn[i].turnon_selectled()
        return self

    def sleep(self): #
        """
        Timer for loops (wait self.tsleep)
        :return: self
        """
        if not self.is_reset():
            time.sleep(self.tsleep) #no sleep in home screen (increase reactivity)
        return self


class class_grpbtn:

    """
    ----class_grpbtn----
    This is class groups btn together for group action
    one element of a grp of class_btn
    In contains fct for detecting btn pressed and can turn on/off led

    If select=True only one btn in the group can be on and the lastest is memorised (selected)
                    in the case of useled=True the led of the selected is turn ON (others are OFF)

    if select=False action occurs when one btn is pressed or released, there is not selection

    If no btn are pressed -1 is return has value, first btn is [0]
    """
    def __init__(self,ibtns,pins,debug=False,select=True):
        self.log = reusables.get_logger('grpbtn '+ str(ibtns), level=logging.DEBUG)

        self.debug=debug#Gives extra info in terminal
        self.btn=[]
        self.err=0
        self.useselect=select #Give a action if btn is released
        self.changed=False #Is true when something changed

        #GRBTN INIT
        for i,tbnt in enumerate(ibtns):
            self.btn.append(class_btn(pins[tbnt],self.debug))
            self.err += self.btn[i].err
        self.n=len(self.btn)
        self.reset_select()

    def checkbtn(self):
        """
        Check if btn is pressed
        :return: self
        """
        self.pressed=[]
        for i in range(self.n):
            self.pressed.append(self.btn[i].ispressed())
        return self
    def list_pressed(self,index=False):
        """
        Return a list of pressed grp (btn id)
        :param index: T/F Index of pressed or list of bool
        :return: Index of pressed or list of bool
        """
        self.checkbtn()
        if not index:
            return self.pressed
        else :
            try :
                return self.pressed.index(True)
            except :
                return -1

    def ispressed(self):
        """
        Return true if a btn is pressed
        :return: Bool
        """
        l=self.list_pressed()
        return any(l)

    def change_led(self,btn,state):
        """
        Change led state
        :param btn: btn[n] to change
        :param state: T/F State of the led
        :return: self
        """
        self.btn[btn].led(state)
        if self.debug :
            self.log.debug ("led "+str(btn)+" is "+str(state))
        return self
    def randomled(self):
        """
        turn on a led choosen randomely
        :return: self
        """
        self.reset_led()
        self.change_led(random.randint(0,self.n-1),True)
        return self
    def reset_led(self):
        """
        Turn OFF all the led of the grp
        :return: self
        """
        for i in range(self.n):
            self.change_led(i,False)
        return self
    def reset_select(self):
        """
        Reset select
        :return: self
        """

        self.waspressed=False
        self.select=-1
        self.pressed=[]
        self.setexernal(False)
        return self
    def setexernal(self,use=False,select=-1):
        self.external = {'use': use, 'select': select}
        if self.debug :
            self.log.debug("set external :"+str(self.external))
    def reset(self):
        """
        global reset : reset led and select
        :return: self
        """
        self.reset_led()
        self.reset_select()
        return self
    def allledon(self):
        """
        Turn all the led of the grp ON
        :return: self
        """
        for i in range(self.n):
            self.change_led(i,True)
        return self
    def turnon_selectled(self):
        """
        Turn on the select led (and off others)
        :return: self
        """
        if not self.external['use']: #TODO need to be changed for more general usage
            self.reset_led()
            if self.select>-1 :
                self.change_led(self.select,True)
        return self

    def action_btn(self):
        """
        Fct action of the grp : looks for what changed, change select, manages LED
        :return: self
        """
        if self.external['use']:
            if self.select != self.external['select']:
                self.select=self.external['select']
                self.changed=True
                if self.debug :
                    self.log.debug("External input " +str(self.select)+ " is pressed")
            else :
                self.changed=False
        elif self.ispressed() :

            b=self.list_pressed(index=True) #only one value is extracted (no double btn)
            self.waspressed=True

            if  self.select != b :
                if self.debug:
                    self.log.debug(str(b)+ " is pressed")
                self.select=b
                self.changed=True
                self.turnon_selectled()
            else:
                self.changed=False
        elif not self.useselect and self.waspressed:
            if self.debug:
                self.log.debug(str(self.select)+ " is released")
            self.changed=True
            self.waspressed=False
            self.select=-1
            self.turnon_selectled()
        else :
            self.changed=False
        return self


class rfid_card(class_grpbtn):
    """
    ----rfid_card (class of class_grpbtn)----
    Management of the rfid reader simulates a group of btns by replacing some fct of grb_btn to interact with rfid card
    Each rfid card is like a btn (with a number) when a card is detected a corresponding number is given

    """
    def __init__(self,cardvsbtn=[],debug=False,select=True,useled=False):
        self.log = reusables.get_logger('rfid', level=logging.DEBUG)

        self.debug=debug #Gives extra info in terminal
        self.changed=False #Is true when something changed
        self.useled=useled#LED are not supported for rfid (possible to add in the future)

        #INIT RDFID
        try :
            self.rfid = RFID(pin_rst=25,pin_irq=24, pin_mode = GPIO.BCM)
            self.err=0
        except:
            self.log.error("Error with RFID Init")
            self.rfid=0
            self.err=1
        self.c2b=cardvsbtn #conversion rfid uid to btn
        self.n=len(self.c2b) #number of cards (or btn)
        self.useselect=select #Give a action if btn is released
        self.retry=2 #number of time we try to find card
        self.detected=False

        self.reset_select()

    def close(self): #Close the rfid
        self.rfid.cleanup()


    def wait(self):
        self.rfid.wait_for_tag()
        return self #If we want to wait a rfid card to be in

    def is_carddetected(self): #Return True if a card is detected and save value to inscrease speed and stability
        error=True
        for i in range(self.retry):
            (err,data) = self.rfid.request()
            if not err:
                (err2, data) = self.rfid.anticoll()
                if not err2:
                    error=False
                    self.uid=data


        if not error:
            if self.debug:
                self.log.debug("Card detected ")
            ans=True
            self.detected=True
        else :
            ans=False
            self.detected=False
        return ans

    def read_card(self): # Return int of the card id
        uid=self.convuid(self.uid)
        if self.debug:
            self.log.debug("Card read UID: "+str(uid))
        return uid

    def convuid(self,uid): #Conv multiple bit of card id into int
        uidtxt=""
        for i,u in enumerate(uid):
            uidtxt = uidtxt + str(u)
        return int(uidtxt)
    def conv_c2b(self,uid): # conv card id to btn id
        try:
            ans=self.c2b.index(uid)
        except:
            ans=-1
        return ans

    #Conversion rfid card2btn (compatible fct are given by class_grpbtn)

    def ispressed(self): #Return true if btn pressed
        return self.is_carddetected()

    def checkbtn(self):#Simulate a group of btn with the number of cards, the card selected is true in the list
        k=[False for i in range(self.n)]
        if self.detected:
            n=self.conv_c2b(self.read_card())
            k[n]=True
        self.pressed=k

        return self

    def reset_led(self): #TODO LED are not supported for rfid
        return self
    def change_led(self,btn,state):
        return self
    def allledon(self):
        return self

class class_btn:

    """
    ----class_btn----
    This is the simplest class it manages the btn and led actions
    one element of the class is ONE btn/led
    In contains fct for detecting btn pressed and can turn on/off led
    A btn is composed of a pinbtn (in GPIO mode)
    If useled = True a pinled need to be provided
    """
    def __init__(self,pincfg,debug=False):


        self.debug=debug
        if pincfg['type']=='btn':
            self.inbtn=class_input(**pincfg['set'],debug=self.debug,use_pud=True)
            self.err = self.inbtn.err
        elif pincfg['type']=='analog':
            self.inbtn=class_analog_bool(**pincfg['set'],debug=self.debug)
            self.err = self.inbtn.err

        elif pincfg['type']=='external':
            self.inbtn=class_external(debug=self.debug)
            self.err=self.inbtn.err

        else :
            self.log.error("Type of btn undefined")
            self.err=1
        if pincfg['useled']:  # True if we want to use a led
            self.useled=True
            self.outled=class_out(**pincfg['setled'],debug=self.debug)
            self.err +=self.outled.err
        else :
            self.useled =False
        if self.err>0 :
            self.log.error("GPIO ERROR")

        self.pressed=False

    def checkbtn(self): #Check if btn is pressed
        """
        Check btn state and update self.pressed
        :return: self
        """
        self.pressed=self.inbtn.state()
        return self
    def ispressed(self):
        """
        Return btn status
        :return: True if pressed / False if not pressed
        """
        self.checkbtn()
        return self.pressed

    def led(self,state):
        """
        Change led state
        :param state: T/F
        :return: self
        """
        if self.useled:
            self.outled.change(state)
            self.ledstate=self.ledison()
        return self

    def ledison(self):
        """
        Return led status
        :return: T/F
        """
        if self.useled :
            return self.outled.status()
        else :
            return False



