import RPi.GPIO as GPIO
import time
import logging
import reusables
from outmng import *
from btnmng import *
GPIO.setmode(GPIO.BCM)# #GPIO Init


class class_smotor:
    def __init__(self, pinENA, pinDIR, pinPUL, rev=6400, homing=False, homeset={}, debug=False):
        # 'pinENA': 14, 'pinDIR': 19, 'pinPUL': 20, 'rev': 6400, 'homing': True,
        # 'homeset': {'type': 'btn', 'pinbtn': 27,'pinled': -1,'useled': False, 'up': True,'trig': 'raising'}

        self.log = reusables.get_logger('smotor ENA:' + str(pinENA), level=logging.DEBUG)
        self.err=0
        outdebug=False
        self.ENA = class_out(pin=pinENA, debug=outdebug)
        self.err+=self.ENA.err
        self.DIR = class_out(pin=pinDIR, debug=outdebug)
        self.err += self.DIR.err
        self.PUL = class_out(pin=pinPUL, debug=outdebug)
        self.err += self.PUL.err
        self.rev = rev
        self.homing = homing
        if self.homing:
            self.HOME = class_btn(pincfg=homeset,debug=outdebug)
            self.err += self.HOME.err
        else:
            self.homeset = 0
        self.debug=debug
        if self.err >0:
            self.log.error("Error with GPIO init !!!")
        else :
            if debug :
                self.log.debug("Step motor init OK")

    def move(self,angle,speed):
        """
        Move the motor for a given angle and speed, quit when finished moving
        :param angle: the angle (float) the sign gives the direction
        :param speed:  speed (in s by turn)
        :return: self
        """
        if self.debug :
            self.log.debug("moving")

        self.ENA.turn_on()
        if angle>=0 :
            self.DIR.turn_off()
        else :
            self.DIR.turn_on()

        npulse=self._angle2pulse(abs(angle))
        tl=self._revtime2pulsetime(speed)
        self.PUL.pulses(npulse,tl,tl)
        if self.debug :
            self.log.debug("stopping")
        self.ENA.turn_off()
        self.DIR.turn_off()

        return self

    def home(self,speed,dir=False,forcemove=False,forceangle=0):

        if self.homing :
            if self.debug:
                self.log.debug("homing")

            if forcemove:
                self.move(forceangle,speed)

            self.ENA.turn_on()
            if not  dir:
                self.DIR.turn_off()
            else:
                self.DIR.turn_on()

            tl = self._revtime2pulsetime(speed)

            while not self.HOME.ispressed(): #TODO can be better and timeout
                self.PUL.pulses(1, tl, tl)

            if self.debug:
                self.log.debug("stopping")
            self.ENA.turn_off()
            self.DIR.turn_off()
        else :
            self.log.error("Homing not activated")
        return self



    def _revtime2pulsetime(self,revtime):
        """
        Convert revolution time (s by turn) into pulse timing
        :param revtime: time to do a turn in s
        :return: time pulses (in s)
        """
        return revtime/(2*self.rev)

    def pulsetime2_revtime(self,pulsetime):
        """
        Convert a pulse time into a rev time (in s)
        :param pulsetime: the pulse length (in s)
        :return: rev speed in (s by turn)
        """
        return pulsetime*(2*self.rev)

    def _pulse2angle(self,pulse):
        """
        Return the angle for given number of pulse
        :param pulse: the number of pulse
        :return: the angle
        """
        return pulse*(360/self.rev)

    def _angle2pulse(self,angle):
        """
        Return the number of pulse for a given angle
        :param angle: angle
        :return: number of pulse (int)
        """
        return int(abs(angle)*self.rev/360)
