import time
import logging
import reusables
from outmng import *
from motormng import *


# out = [
#     {'type': 'out', 'args':{'pin': 13,}},
#     {'type': 'out', 'args':{'pin': 5}},
#     {'type': 'pulses', 'args': {'pin': 6,'npulse': 4, 't_on': 0.2, 't_off': 0.2}},
#     {'type': 'smotor', 'args': {'pinENA': 14, 'pinDIR': 19, 'pinPUL': 20, 'rev': 6400,
#                                               'homing': True, 'homeset': {'type': 'btn','set': {'pin': 27, 'pudup': True,'trig': 'falling'},'useled': False}}},
# ]
# timers_channel = [
#     {'out_use': 0,
#      'start': {'source': 'external', 'source_inp': 0, 'from': 'end', 't': 20, 'use': {'fct': 'start', 'args': {}}},
#      'stop': {'source': 'external', 'source_inp': 0, 'from': 'end', 't': 16, 'use': {'fct': 'stop', 'args': {}}}},
#     {'out_use': 1,
#      'start': {'source': 'external', 'source_inp': 0, 'from': 'end', 't': 25, 'use': {'fct': 'start', 'args': {}}},
#      'stop': {'source': 'external', 'source_inp': 0, 'from': 'end', 't': 16, 'use': {'fct': 'stop', 'args': {}}}},
#     {'out_use': 2,
#      'start': {'source': 'external', 'source_inp': 0, 'from': 'end', 't': 20, 'use': {'fct': 'start', 'args': {}}},
#      'stop': {'source': 'ntimes', 'source_inp': 1, use': {'fct': '', 'args': {}}}},
#     {'out_use': 3,
#      'start': {'source': 'external', 'source_inp': 0, 'from': 'end', 't': 16,
#                'use': {'fct': 'home', 'args': {'speed': 2, 'dir': False, 'forcemove': True, 'forceangle': 20}}},
#      'stop': {'source': 'ntimes', 'source_inp': 1 use': {'fct': '', 'args': {}}}}
# ]


class class_timersout:
    """
    This class provide several timers for executing task at precise timing
    an action start and stop can be configured
    external timer are external time that can be configure outside the class
    internal are between timer (so one timer can be relative to another)
    """

    def __init__(self, timers_channel, outs, debug=False):
        self.log = reusables.get_logger('timersout', level=logging.DEBUG)
        self.debug = debug
        self.n = len(timers_channel)
        self.act = []
        for i, timer in enumerate(timers_channel):
            self.act.append({
                'action': timer_action(outs[timer['out_use']], timer,debug=self.debug),
                'start': timer_timing(timer['start']),
                'stop': timer_timing(timer['stop']),
                'tstart': 0,
                'tstop': 0,

            })
        self.t = {}
        self.length = {'internal': {}, 'external': {}}
        if self.debug:
            self.log.debug("Init OK with n: " + str(self.n))

    def set_length(self, length, timer=0, type='external'):
        """
        Configure the known length for a timer this will be use for configuring the end setting
        you don't need it if you use only start time
        :param length: the length in s
        :param timer: the selected timer (int)
        :param type: configure external or internal timer,
        :return: self
        """
        if self.debug:
            self.log.debug("Set length timer :" + str(timer) + " length :" + str(length) + " type :" + type)
        self.length[type][str(timer)] = length
        for i in range(self.n):
            self.act[i]['start'].set_length(length, timer, type)
            self.act[i]['stop'].set_length(length, timer, type)

        return self

    def set_t(self, t, timer=0):
        """
        Use this function to enter the time for a specific timer (0 by default)
        :param t: the time in s
        :param timer: the timer you want to input
        :return: self
        """
        self.t[str(timer)] = t
        for i in range(self.n):
            self.act[i]['start'].set_t(t, timer, type='external')
            self.act[i]['stop'].set_t(t, timer, type='external')
        return self


    def action(self):
        """
        Put this function in your code to manage all the trigger (need to do a set_t before)
        :return: self
        """

        self._make_action()
        for i in range(self.n):
            if self.act[i]['start'].need_trigged():
                for ti in range(self.act[i]['stop'].give_ntimes()):
                    # TODO for the moment n times stop the main prog until it's done
                    self.start_i(i)

            if (self.act[i]['stop'].need_trigged() and self.act[i]['start'].trigged) or \
                    (self.act[i]['start'].trigged and self.act[i]['start'].isntimes()):
                self.stop_i(i)
        return self
        # self._make_action('internal')

    def reset(self):
        """
        Reset all the channel so they can be trigged again
        :return: self
        """
        for i in range(self.n):
            self.act[i]['start'].reset()
            self.act[i]['stop'].reset()
            self.act[i]['tstart'] = 0
            self.act[i]['tstop'] = 0
        if self.debug:
            self.log.debug("reset done")
        return self

    def start_i(self, i):
        """
        Start a specific channel i
        :param i: channel (int)
        :return: self
        """
        self.act[i]['action'].start()
        self.act[i]['start'].was_trigged()
        self.act[i]['tstart'] = time.time()
        if self.debug:
            self.log.debug("channel i :" + str(i) + " started")
        return self
    def force_starti(self,i):
        self.act[i]['action'].start()
        return self
    def force_stopi(self,i):
        self.act[i]['action'].stop()
        return self

    def stop_i(self, i):
        """
        Stop a specific channel i
        :param i: channel (int)
        :return: self
        """
        self.act[i]['action'].stop()
        self.act[i]['stop'].was_trigged()
        self.act[i]['tstop'] = time.time()

        if self.debug:
            self.log.debug("channel i :" + str(i) + " stoped")
        return self

    def _make_action(self, type='external'):
        for timer, t in self.t.items():
            for i in range(self.n):
                self.act[i]['start'].action(int(timer), type)
                self.act[i]['stop'].action(int(timer), type)
        return self


class timer_timing:
    """
    Child class for controling a channel of timming
    """

    def __init__(self, timer):
        self.source = timer['source']
        self.source_inp = timer['source_inp']
        try:
            self.tact = timer['t']
            self.tfrom = timer['from']
        except:
            self.tact = -1
            self.tfrom = 'undef'
        self.reset()


    def reset(self):
        """
        Restore all the trigger to zero
        :return: self
        """
        self.set_t(0,timer=self.source_inp,type=self.source)
        self.ntrig = False
        self.trigged = False

        return self

    def isforme(self, timer, type):
        """
        Return true if the timer and type are for this channel
        :param timer: timer name (int)
        :param type: type of trigger
        :return: Bool
        """
        if timer == self.source_inp and type == self.source:
            return True
        else:
            return False

    def set_length(self, length, timer, type):
        """
        Set the length and calculate the time of action if from end
        :param length: the total length (s)
        :param timer: the timer and type will be controled
        :param type:
        :return: self
        """
        if self.isforme(timer, type):
            if self.tfrom == 'end':
                self.tact = length - self.tact
        return self

    def set_t(self, t, timer, type):
        """
        Set the time of this channel
        :param t: in s
        :param timer: timer and type will be controled for this channel
        :param type:
        :return:
        """
        if self.isforme(timer, type):
            self.t = t
        return self

    def action(self, timer, type):
        """
        Will change the trigger if needed
        :param timer: timer and type will controled
        :param type:
        :return: self
        """
        if self.tfrom !='undef' and self.isforme(timer, type):
            if not self.trigged and self.t >= self.tact:

                self.ntrig = True
        return self

    def need_trigged(self):
        """
        Return true if trigger is needed for this channel
        :return: bool
        """
        return self.ntrig

    def was_trigged(self):
        """
        Use this fct if the channel was triggered
        :return: self
        """
        self.trigged = True
        self.ntrig = False
        return self



    def give_source_inp(self):
        """
        Return the source input parameter
        :return: parameter (int)
        """
        return self.source_inp

    def give_ntimes(self):
        """
        Return the number of time this channel should start()
        :return: (int)
        """
        if self.isntimes():
            return self.source_inp
        else:
            return 1

    def isntimes(self):
        """
        Return true if the channel is a ntimes mode
        :return: bool
        """
        if self.source == 'ntimes':
            return True
        else:
            return False

    def isext(self):
        """
        Return true if the channel is in external mode
        :return: bool
        """
        if self.source == 'external':
            return True
        else:
            return False

    def isint(self):
        """
        Return true if the channel is in internal mode
        :return:  bool
        """
        if self.source == 'internal':
            return True
        else:
            return False


class timer_action:
    """
    Define a generic start() and stop() for a given class and fct
    """

    def __init__(self, classout, timer,debug=False):
        self.debug=debug
        self._act = self.callclass(classout)
        self.set_start = timer['start']['use']['args']
        self.set_stop = timer['stop']['use']['args']
        self._f_start = self.callfct(self._act, timer['start']['use']['fct'])
        self._f_stop = self.callfct(self._act, timer['stop']['use']['fct'])

    def start(self):
        """
        Call Start  function with start kwargs
        :return: self
        """
        self._f_start(**self.set_start)
        return self

    def stop(self):
        """
        Call stop fct with stop kwargs
        :return: self
        """
        self._f_stop(**self.set_stop)
        return self

    def callclass(self, input):
        """
        return the class_type with kwargs init
        :param input: {type and args} class name
        :return: the class
        """
        classname = input['type']
        kwargs = input['args']
        kwargs['debug']=self.debug
        return eval('class_' + classname + '(**kwargs)', globals(), {'kwargs': kwargs})

    def callfct(self, obj, fct):
        """
        Return the function of a given obj (class=
        :param obj: class name
        :param fct: function name (str)
        :return: fct obj
        """
        if fct == '':
            return self.null
        else:
            return getattr(obj, fct)

    def null(self):
        """
        Define a null function
        :return: a fct
        """
        return self
