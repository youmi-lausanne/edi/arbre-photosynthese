
from string import Template
import yaml
import reusables
import logging
import time

from dispmng import dispmng
from btnmng import class_mngbtn
from timersmng import class_timersout

class pcb_Parbre:  #Reglage pour le pcb et autre setup

         #Pin on PCB (btn,led)

        btn=[
            {'type':'btn','set':{'pin' : 4, 'pudup':True,'trig':'falling'},  'useled' : False},
            {'type': 'analog','set':{'board':'1115','usage' : 0,'gain':16,'threshold':{'value':100,'unit':'B','abs':True}}, 'useled': False,},
            {'type':'btn','set':{'pin' : 18, 'pudup':True,'trig':'raising'}, 'useled' : True,'setled':{'pin' : 12}},
            ]
        grp=[[0],[1],[2]] #Defini les groupes de bouton

         #OUt : Fan , Brum 220v, brum start btn
        out = [
             {'type': 'out', 'args':{'pin': 13,}},
             {'type': 'out', 'args':{'pin': 5}},
             {'type': 'pulses', 'args': {'pin': 6,'npulse': 4, 't_on': 0.2, 't_off': 0.2}},
             {'type': 'smotor', 'args': {'pinENA': 14, 'pinDIR': 19, 'pinPUL': 20, 'rev': 6400,
                                                       'homing': True, 'homeset': {'type': 'btn','set': {'pin': 27, 'pudup': True,'trig': 'falling'},'useled': False}}},
         ]
        timers_channel = [
             {'out_use': 0,
              'start': {'source': 'external', 'source_inp': 0, 'from': 'start', 't': 10, 'use': {'fct': 'start', 'args': {}}},
              'stop': {'source': 'external', 'source_inp': 0, 'from': 'end', 't': 30, 'use': {'fct': 'stop', 'args': {}}}},
             {'out_use': 1,
              'start': {'source': 'external', 'source_inp': 0, 'from': 'start', 't': 5, 'use': {'fct': 'start', 'args': {}}},
              'stop': {'source': 'external', 'source_inp': 0, 'from': 'end', 't': 30, 'use': {'fct': 'stop', 'args': {}}}},
             {'out_use': 2,

              'start': {'source': 'external', 'source_inp': 0, 'from': 'start', 't': 6, 'use': {'fct': 'start', 'args': {}}},
              'stop': {'source': 'ntimes', 'source_inp': 1, 'use': {'fct': '', 'args': {}}}},
             {'out_use': 3,
              'start': {'source': 'external', 'source_inp': 0, 'from': 'end', 't': 13,
                        'use': {'fct': 'home', 'args': {'speed': 2, 'dir': False, 'forcemove': True, 'forceangle': 20}}},
              'stop': {'source': 'ntimes', 'source_inp': 1 ,'use': {'fct': '', 'args': {}}}}
         ]

        updateoff=False #Refait un update quand un bouton passe off

        usesolbtn=False
        correct=[] #defini les boutons corrects ds chaque grp (1er : 0)
        solbtn=() # dans btn lequel est la solution
        rfid=False
        useFS=True # Utilise full screen
        screen_center=(960,540) #centre de l'ecran (si useFs False)
        screen_size=(1002,1080) #Taille de l'ecran (si useFs False)



class mng_outputs():
    def __init__(self,debug=False,pcb=pcb_Parbre):
        self.log = reusables.get_logger('mng_outputs', level=logging.DEBUG)
        self.debug = debug
        self.err=0

        #TODO add error handling
        self.timers=class_timersout(timers_channel=pcb.timers_channel,outs=pcb.out,debug=self.debug)
        if self.debug:
            self.log.debug("Init outputs done")

    def forcehoming(self):
        # Homing step motor
        if self.debug:
            self.log.debug("force homing step motor")
        self.timers.force_starti(3)
    def makefinale(self,t):
        self.timers.set_t(t)
        self.timers.action()
        return self

    def reset(self):
        self.timers.reset()
        return self

class parbre_mngbtn(class_mngbtn):

    def action_btn_unique(self):
        """
        check if something changed, this function is different as is prevent double changes between groups

        :return: self
        """
        changed=[]
        unique=False
        for i in range(self.n):
            ans = self.gbtn[i].ispressed()
            if not unique and ans :
                self.gbtn[i].action_btn()
                changed.append(self.gbtn[i].changed)
                unique=True
            elif unique and ans:
                if self.debug:
                    self.log.debug("double press detected only one is stored")
            else :
                self.gbtn[i].action_btn()
                changed.append(self.gbtn[i].changed)
        self.changed=any(changed)
        return self

    def is_changed_unique(self, sol=True):
        """
        Return true if an input changed
        :param sol: does it include solice btn (T/F)
        :return: T/F
        """
        self.action_btn_unique()

        if self.changed :
            self.tla = time.time()
        #    if self.ansreset:
         #       self.turnon_selectled()  # Reset leds when we were in home mode
            if self.debug:
                self.log.debug("something changed")
            return True
        else:
            return False

class mng_parbre(dispmng):
    # Correspondance bouton image
    #cor : matrice de correlation entre bouton et esv dans les fichiers images
    cor=[0,1,2]
    tab = [(1,0,0),(0,1,0),(0,0,1),(0,1,2),(0,2,1),(0,2,2),(1,2,2),(1,2,0),(2,1,2),(2,0,1),(2,1,0),(2,2,1),(2,2,2)]
    gotoend = [(1, 2, 2), (2, 1, 2), (2, 2, 1)] #Input combinaison that lead to end movie
    end = [(2, 2, 2)] #Combinaison corresponding to the end movie
    def __init__(self,imgfolder="media",debug=False,pctest=False,pcb=pcb_Parbre):
        self.log = reusables.get_logger('MNG', level=logging.DEBUG)

        self.pcb=pcb
        self.ngrp=len(self.pcb.grp)
        self.nbtn=len(self.pcb.grp[0])
        self.imgfolder=imgfolder
        self.debug=debug
        self.pctest=pctest
        super().__init__(self)
        #loading movie
        self.nimg=len(self.tab)


        if self.debug:
             self.log.debug("MNG : nACT="+str(self.nimg))
        self.media=[]
        #Loading movie timer file
        try :
            with open("./movie.yml", 'r') as ymlfile:
                cfg = yaml.load(ymlfile)
            self.err = 0
        except :
            self.err = 1
            self.log.error("Error loading the config file")

        self.filetemp = Template("ACT_E$E-V$V-S$S.mp4")
        path="./"+self.imgfolder+"/"
        for n in range(self.nimg):
            mat=self.n2evs(n)
            filename=self.filetemp.substitute(E=mat[0],V=mat[1],S=mat[2])
            (media,err)=self.loadmedia(path+filename)
            if mat in self.gotoend + self.end :
                media['timer'] =media['duration']
            else :
                try :
                    media['timer']=cfg[filename]['no_act_wait']
                except :
                    media['timer'] =-1
                    err+=1
                    self.log.error("Error in the config file")
            self.err += err
            self.media.append(media)
            if self.debug :
                self.log.debug("Loading :"+filename)

        #standby img loading
        (stdby,err)=self.loadmedia(path+"standby.mp4")
        stdby['timer'] =0 #no timer for standby
        self.err += err
        if self.err==0:
            self.stdmov=stdby
            if self.debug:
                self.log.debug("Loading :"+"standby.mp4")

        if self.err>0 :
            self.log.error("Error with loading media")
        elif self.debug :
            self.log.debug("media loaded")

        #Var init
        self.movie_timer=0
        self.pressed = []
        self.reseted=False
    def btn2img(self,btn):
        """
        conv [btn] into (e,s,v)
        :param btn: Btns setting
        :return: tuple of ESV
        """
        esv=[]
        for i in range(self.ngrp):
          #-1 qd non press = 0 img
            esv.append(btn[self.cor[i]]+1)
        return tuple(esv)

    def giveimg(self,esv): #return an img for a given (e,s,v)
        n=self.evs2n(tuple(esv))
        return self.media[n]

    def print_media(self,btns):
        """
        print image according to btns
        :param btns: actual btn setting
        :return: self
        """
        self.reseted=False
        esv=self.btn2img(btns)
        img=self.giveimg(esv)
        self.movie_timer=img['timer']
        if self.debug:
            self.log.debug("playing "+str(esv)+"timer : "+str(self.movie_timer))
        self.dispmedia(img)
        return self

    def waiting(self,t=0):
        """
        waiting screen (home)
        :param t: actual time (not used here)
        :return: self
        """
        #self.movie.quit()
        self.dispmedia(self.stdmov,prt=False)
        self.movie.play(loop=True)
        return self

    def n2evs(self,n):
        """
        Convert a [n] into a (e,v,s)
        :param n: integer
        :return: tuple
        """

        return self.tab[n]

    def evs2n(self,evs): #
        """
        conv  a (e,v,s) into [n]
        :param evs: tuple
        :return: integer
        """
        return self.tab.index(tuple(evs))

    def need_reset(self):
        """
        Return True if the app need reset according to movie player
        :return: T/F
        """
        if not self.reseted and not self.movie_isplaying():
            ans=True
        else :
            ans=False

        return ans


    def reset(self):
        """
        In case of reset, stop tje player and init variables
        :return: self
        """
        self.movie_quit()
        self.movie_timer=0
        self.movie=[]
        self.reseted=True
        return self
    def timer_over(self,t):
        """
        Return true if timer for the movie is over
        :param t: actual time
        :return: T/F
        """
        return self.movie_timer <= t
