import time
import reusables
import logging
from MNG_PARBRE import *

log = reusables.get_logger('main', level=logging.DEBUG)

debug=True #extra display on console

# Init
disp=mng_parbre(debug=debug,pctest=False)
disp.add_txt(txt="Initialize inputs")
app=parbre_mngbtn(pcb=pcb_Parbre,tsleep=0.1,debug=debug)
disp.add_txt(txt="Initialize outputs")
outs=mng_outputs(debug=debug)
disp.add_txt(txt="Homing motor")
outs.forcehoming()



if disp.err>0  or app.err>0 or outs.err>0:
    #display not working screen
    disp.pnotworking()
    while not disp.doquit():
        time.sleep(0.2)
    log.error("python script ended with errors")
    quit()
else :
    #display ready screen
    disp.pready()
    if debug:
        log.debug("Ready start in 5s")
    time.sleep(5)
    disp.clean_screen()

if debug:
    log.debug("Starting ...")

outs.timers.set_length(disp.giveimg(disp.end[0])['duration'])  # define the length of the timer with the length of the last movie

disp.waiting() #display init screen at beginning
btns=[]

while not disp.doquit(): # infinite loop
    t=app.last_change() #timing
    if disp.timer_over(t) and (app.is_changed_unique() or tuple([x+1 for x in btns]) in disp.gotoend) : # if something changed and timer for the movie is over (TODO check that is changed is not called until timer over is true)
        if 0 in btns :
            if debug:
                log.debug("Return btns :"+str(btns))
            app.external_on(grp=btns.index(0),select=1)
            app.gbtn[btns.index(0)].action_btn()
            t=0
            app.tla=time.time()

        btns=app.list_pressed() #return the list of selected btn (-1 if not selected)

        if debug:
                log.debug("Main : btns=" + str(btns))
        if tuple([x+1 for x in btns]) in disp.tab :
            disp.movie_quit() #we close previous movie
            disp.print_media(btns) #Display movie
        elif debug:
            log.debug("This possibility is not playable")
    if tuple([x + 1 for x in btns]) in disp.end : #Do the final on the outputs
        outs.makefinale(t)

    if disp.need_reset() and not (tuple([x+1 for x in btns]) in disp.gotoend):
        if debug:
            log.debug("Reset needed")
        app.reset() # reset if needed
        disp.reset()
        btns = [] #erase btn for a fresh start
        outs.reset()
        if debug:
            log.debug("Diplay standby screen")
        disp.waiting()  # display the home screen
    app.sleep() #timer for loop
disp.movie_quit()
log.debug("python script ended")
quit()
