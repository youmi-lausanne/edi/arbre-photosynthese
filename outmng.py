import RPi.GPIO as GPIO
import time
import logging
import reusables
GPIO.setmode(GPIO.BCM)# #GPIO Init

class class_out() :

    """
    ---class_out___
    Manages output channel

    """
    def __init__(self,pin,debug=False,offlow=True):
        """
        Initiate the class out class
        :param pinout: the pin on the raspberry used as output (in BCM mode)
        :param debug:  if True extra outputs
        :param offlow: if False, Off state means High and On Low (otherwise normal behavior)
        :return: self
        """
        self.pin=pin
        self.log = reusables.get_logger('out '+ str(self.pin), level=logging.DEBUG)
        self.debug=debug

        self.err=0
        if offlow :
            self.on = GPIO.HIGH
            self.off = GPIO.LOW
        else :
            self.on=GPIO.LOW
            self.off=GPIO.HIGH

        try :
            GPIO.setup(self.pin,GPIO.OUT,initial=self.off)

            if(self.debug):
                self.log.debug("pinout allocated: "+str(self.pin))


        except :
            self.log.debug("GPIO ERROR with pinout : " +str(self.pin))
            self.err=1

        self.state=False

    def turn_on(self):
        """
        turn on the channel
        :return:  self
        """
        GPIO.output(self.pin, self.on)

        self.state=True
        if self.debug:
            self.log.debug("Turn on")
        return self

    def turn_off(self):
        """
Turn off the channel
        :return: self
        """
        GPIO.output(self.pin, self.off)

        self.state=False
        if self.debug:
            self.log.debug("Turn off")
        return self

    def change(self,state):
        """
        Change the channel with state
        :param state: True channel ON False channel OFF
        :return: self
        """
        if self.debug:
            self.log.debug("Changed")
        if state :
            self.turn_on()
        else :
            self.turn_off()
        return self

    def inv(self):
        """
        Inverse the state of the output
        :return: self
        """
        if self.debug:
            self.log.debug("Inversed")
        if self.state:
            self.turn_off()
        else:
            self.turn_on()
        return self


    def status(self):
        """
        Return the exact state of the output by using GPIO readout
        :return: The exact status of the output
        """
        return GPIO.input(self.pin)
    def start(self):
        self.turn_on()
        return self

    def stop(self):
        self.turn_off()
        return self



    def pulses(self,npulse,t_on,t_off):
        """
        Generate pulses
        :param npulse: nbr of pulses
        :param t_on: time on (s)
        :param t_off: time off (s)
        :return: self
        """
        if self.debug:
            self.log.debug("Pulses On")
        for i in range(npulse):
            self.turn_on()
            time.sleep(t_on)
            self.turn_off()
            time.sleep(t_off)

        return self

    def reset(self):
        self.turn_off()
        return self




class class_pulses(class_out): #TODO Pulse with t so software is not locked
    """
    Generates a train of pulses (stop the software for that) mostly designed to use with timer
    """
    def __init__(self,npulse,t_off,t_on,**kwargs):
        """

        :param pinout: BCM output
        :param npulse: number of pulses
        :param t_off: length of the off state (m)
        :param t_on:  length of the on state (m)
        :param stimer: Timer (s)
        :param debug: extar display (T/F)
        :param offlow: OFF = Low state (F) OFF=High State (T)
        """
        super().__init__(**kwargs)
        self.Set={'npulse':npulse,'t_off':t_off,'t_on':t_on}


    def start(self):
        self.pulses(**self.Set)
        return self





