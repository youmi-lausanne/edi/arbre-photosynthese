import yaml
from string import Template
class NoAliasDumper(yaml.Dumper):
    def ignore_aliases(self, data):
        return True

temp=Template("ACT_E$E-V$V-S$S.mp4")
default={'no_act_wait':10}
cfgfile="movie.yml"


tab=[(1,0,0),(0,1,0),(0,0,1),(0,1,2),(0,2,1),(0,2,2),(1,2,2),(1,2,0),(2,1,2),(2,0,1),(2,1,0),(2,2,1),(2,2,2)]

gotoend = [(1, 2, 2), (2, 1, 2), (2, 2, 1)]  # Input combinaison that lead to end movie
end = [(2, 2, 2)]  # Combinaison corresponding to the end movie
nohere=[(0,0,0)]

ignore=gotoend+end+nohere
print("ignore : ",ignore)
cfg={}
for i in range(len(tab)):
    (E,V,S)=tab[i]
    print((E,V,S))
    if (E,V,S) in ignore:
        continue
    else :
        cfg[temp.substitute(E=E,V=V,S=S)]=default

print("n=",len(cfg))



file = open(cfgfile,"w")
yaml.dump(cfg,file,  Dumper=NoAliasDumper)
file.close()

print("file written : ",cfgfile)



