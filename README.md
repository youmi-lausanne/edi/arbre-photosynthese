2018 EDI Photosynthese Mandat Externe	
=================


Explique la photosynthèse : CO2, Eau, Lumière= Sucre = Production de bois


[Acces à la documnentation complete](./doc/)

---

# Inputs/Outputs

## Inputs :
* Le CO2 est détecté par une hélice connectée à un moteur (l’enfant souffle dans un tube)
* La lumière est connecté à un bouton poussoir 
* L’eau est amené par une pompe à eau détecté par une jauge de pression 

## Outputs :
* La lumiere s'allume
* Un ventilateur simule l'air
* L’eau est aspergé par un brumisateur 
* Un moteur pas à pas tourne

## Données Techniques :
*  Lorsqu'une action à été faite, on **ignore** si rejoué
*  Lorsqu'une action est faites pendant la lecture d'une vidéo, on **ignore ** l'action jusqu'à ce que l'on soit dans la partie *attente* de la vidéo (timer)
*  La lumiére s'allume imédiatement lors de l'appye sur le bouton
*  L'écran d'accueil est une vidéo qui tourne en boucle
*  Si il n'y a pas d'action jusqu'à un certain delais on retourne à l'écran d'accueil idem à la fin de le scene finale, lors de ce retour toutes les entrées/sorties sont remises à zero



# Principe 

## En Bref 


Chaque input génère une vidéo qui est ensuite mémorisé (vidéo d’attente)  jusqu’à avoir les 3 inputs : vidéos résultat.
Lors de la vidéo finale  le bruminisateur, le ventillateurse mettent en route et le moteur pas à pas fait un tour.
Un écran de veille est une vidéo qui tourne en permanence tant qu'il n'y a pas eu d'action

# Fichiers sources :

## btnmng.py
Classes pour le management des inputs boutons
## dispmng.py
Classes pour le display 
## gen_inifile.py
Génére le fichier de configuration movie.yml
## inputmng.py
Classes pour la gestion des inputs
## MNG_PARBRE.py
Classes adapatés pour le montage ainsi que de la configuration
## motormng.py
Classes pour le management de moteur
## movie.yml
Fichier de configuration des vidéos
## outmng.py
Classses pour le management d'output
## run.py
Fichier Principal du montage
## setup.py
Fichier de configuration (modules nécessaires)
## testanalog.py
Fichier test pour entrée analogiaue
## testmotor.py
Fichier test pour le step motor
## timersmng.py
Classes pour la gestion des timers